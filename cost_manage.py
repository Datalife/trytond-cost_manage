# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin, tree
from trytond.pyson import Eval, Bool, Or
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext
import copy

SEPARATOR = ' / '


class MissingFunction(fields.Function):
    '''Function field that will raise the error
    when the value is accessed and is None'''

    def __init__(self, field, error, getter, setter=None, searcher=None,
            loading='lazy'):
        super(MissingFunction, self).__init__(field, getter, setter=setter,
            searcher=searcher, loading=loading)
        self.error = error

    def __copy__(self):
        return MissingFunction(copy.copy(self._field), self.error, self.getter,
            setter=self.setter, searcher=self.searcher)

    def __deepcopy__(self, memo):
        return MissingFunction(copy.deepcopy(self._field, memo), self.error,
            self.getter, setter=self.setter, searcher=self.searcher)

    def __get__(self, inst, cls):
        value = super(MissingFunction, self).__get__(inst, cls)
        if inst is not None and value is None:
            raise UserError(gettext(self.error, model=inst.name,
                id=inst.id))
        return value


class CostAllocationMixin(object):

    company = fields.Many2One('company.company', 'Company', required=True,
        select=True)
    type_ = fields.Many2One('cost.manage.cost.allocation.procedure',
        'Type', required=True, select=True)
    source = fields.Reference('Cost source',
        selection='get_source_name', required=True, select=True)
    amount = fields.Numeric('Cost amount',
        digits=(16, Eval('currency_digits', 2)), required=True,
        depends=['currency_digits'])
    currency = fields.Function(
            fields.Many2One('currency.currency', 'Currency'),
            'get_currency')
    currency_digits = fields.Function(
            fields.Integer('Currency Digits'),
            'get_currency_digits')
    date = fields.Date('Date', required=True, select=True)
    category = fields.Many2One('cost.manage.category',
        'Cost category', required=True, readonly=True, select=True,
        ondelete='RESTRICT', domain=[
            ('childs', '=', None)
        ])
    root_category = fields.Many2One('cost.manage.category', 'Root Category',
        required=True, ondelete='RESTRICT', select=True,
        domain=[('parent', '=', None)])
    source_code = fields.Function(
        fields.Char('Source code'), 'get_source_data')
    source_concept = fields.Function(
        fields.Char('Source concept'), 'get_source_data')

    def get_currency(self, name):
        return self.company.currency.id

    def get_currency_digits(self, name):
        return self.currency.digits

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Category = pool.get('cost.manage.category')

        categories = {}
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if values['category'] not in categories:
                root_cat = Category(values.get('category')).root
                categories[values.get('category')] = \
                    root_cat and root_cat.id or values['category']
            values['root_category'] = categories[values.get('category')]
        return super().create(vlist)

    @classmethod
    def _get_source_name(cls):
        return ['']

    @classmethod
    def get_source_name(cls):
        Model = Pool().get('ir.model')
        models = cls._get_source_name()
        models = Model.search([
                ('model', 'in', models),
                ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @classmethod
    def get_source_data(cls, records, names):
        record_ids = {r.id: None for r in records}
        res = {name: record_ids.copy() for name in names}
        return res


class CostSourceMixin(object):

    cost_category = fields.Function(
        fields.Many2One('cost.manage.category', 'Cost category'),
        'get_cost_category')
    cost_override = fields.Boolean('Cost override', readonly=True)

    def get_cost_category(self, name):
        pass

    @staticmethod
    def default_cost_override():
        return True


class CostCategory(DeactivableMixin, tree(separator=SEPARATOR),
        ModelSQL, ModelView):
    '''Cost Manage Category'''
    __name__ = 'cost.manage.category'

    name = fields.Char('Name', translate=True)
    parent = fields.Many2One('cost.manage.category', 'Parent',
        select=True, left='left', right='right', ondelete="RESTRICT")
    childs = fields.One2Many('cost.manage.category', 'parent',
        'Children')
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)

        table = cls.__table_handler__(module_name)
        if table.column_exist('kind'):
            table.drop_column('kind')

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @classmethod
    def validate(cls, categories):
        super().validate(categories)
        for category in categories:
            category.check_name()

    @property
    def root(self):
        categories, = self.__class__.search([
                ('parent', '=', None),
                ('parent', 'parent_of', [self.id])
            ]) or [None]
        return categories

    def check_name(self):
        if SEPARATOR in self.name:
            raise UserError(
                gettext('cost_manage.msg_cost_manage_category_wrong_name',
                    category=self.name, separator=SEPARATOR))


class CategorizedBaseMixin(object):

    cost_category = fields.Many2One('cost.manage.category',
        'Cost Category', domain=[('childs', '=', None)])
    cost_category_used = MissingFunction(
        fields.Many2One('cost.manage.category', 'Cost Category Used'),
        'cost_manage.msg_cost_manage_missing_cost', 'get_cost')

    def get_cost(self, name):
        cost_category = getattr(self, name[:-5])
        return cost_category.id if cost_category else None


class CategorizedMixin(CategorizedBaseMixin):
    '''
    Mixin's children must initialize cost_concept_category
    '''
    costs_category = fields.Boolean('Costs Category')

    @classmethod
    def __setup__(cls):
        super(CategorizedMixin, cls).__setup__()
        cls.cost_concept_category.states.update({
            'invisible': ~Bool(Eval('costs_category'))
        })
        cls.cost_concept_category.domain.append(('cost', '=', True))
        cls.cost_concept_category.depends.append('costs_category')
        cls.cost_category.states.update({
            'invisible': Bool(Eval('costs_category'))
        })
        cls.cost_category.depends.append('costs_category')

    def get_cost(self, name):
        if self.costs_category:
            cost_category = self.cost_concept_category.__getattr__(name)
        else:
            cost_category = getattr(self, name[:-5])
        return cost_category.id if cost_category else None


class CategorizedCategoryMixin(CategorizedBaseMixin):
    '''
    Mixin's children must have hierarchy
    '''
    cost = fields.Boolean('Cost', select=True,
        states={
            'readonly': Bool(Eval('childs', [0])) | Bool(Eval('parent')),
            },
        depends=['parent'])
    cost_parent = fields.Boolean('Use Parent\'s costs',
        states={
            'invisible': ~Eval('cost', False),
            },
        depends=['cost'],
        help='Use the costs defined on the parent category')

    @classmethod
    def __setup__(cls):
        super(CategorizedCategoryMixin, cls).__setup__()
        cls.cost_category.states['invisible'] = (Eval('cost_parent')
            | ~Eval('cost', False))
        cls.cost_category.depends.extend(['cost_parent', 'cost'])
        cls.parent.domain = [
            ('cost', '=', Eval('cost', False)),
            cls.parent.domain or []]
        cls.parent.states['required'] = Or(
            cls.parent.states.get('required', False),
            Eval('cost_parent', False))
        cls.parent.depends.extend(['cost', 'cost_parent'])

    def get_cost(self, name):
        if self.cost_parent:
            # Use __getattr__ to avoid raise of exception
            cost_category = self.parent.__getattr__(name)
        else:
            cost_category = getattr(self, name[:-5])
        return cost_category.id if cost_category else None


class CostDocumentableMixin(object):

    cost_override = fields.Boolean('Cost override', readonly=True)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['cost_override'] = True
        return super(CostDocumentableMixin, cls).copy(records, default=default)

    @staticmethod
    def default_cost_override():
        return True
