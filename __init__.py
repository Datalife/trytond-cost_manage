# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import cost_allocation_procedure
from . import cost_manage
from . import configuration


def register():
    Pool.register(
        cost_allocation_procedure.CostAllocationProcedureType,
        cost_allocation_procedure.CostAllocationProcedure,
        cost_allocation_procedure.CostAllocationPattern,
        cost_allocation_procedure.CostAllocateParam,
        cost_manage.CostCategory,
        configuration.Configuration,
        module='cost_manage', type_='model')
    Pool.register(
        cost_allocation_procedure.CostAllocate,
        module='cost_manage', type_='wizard')
