# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, ModelSingleton


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Cost Manage Configuration'
    __name__ = 'cost.manage.configuration'
