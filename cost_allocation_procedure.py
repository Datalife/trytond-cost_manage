# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Eval, Bool
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext
import sys
import traceback


class CostAllocationProcedure(ModelView, ModelSQL):
    """Cost allocation procedure"""
    __name__ = 'cost.manage.cost.allocation.procedure'

    code = fields.Char('Code', required=True, select=True)
    name = fields.Char('Name', required=True, translate=True, select=True)
    procedure_type = fields.Many2One(
        'cost.manage.cost.allocation.procedure.type', 'Type',
        select=True, ondelete='RESTRICT')

    @classmethod
    @ModelView.button_action('cost_manage.wizard_cost_allocate')
    def allocate(cls, records):
        pass

    @classmethod
    def allocate_pending(cls, records):
        if isinstance(records, int):
            records = [records]
        if isinstance(records, list) and isinstance(records[0], int):
            records = cls.browse(records)
        for record in records:
            record.compute_allocations(pending=True)

    def compute_allocations(self, **cost_params):
        try:
            Transaction().database.lock(
                Transaction().connection, self._to_lock())
        except Exception:
            Transaction().rollback()
            traceback.print_exc(file=sys.stdout)
            raise UserError(gettext('msg_cost_manage_cost_allocation_procedure'
                '_cant_lock_allocation_table'))
        return self._compute_allocations(**cost_params)

    def _to_lock(self):
        raise NotImplementedError

    def _compute_allocations(self, lower_date=None, upper_date=None,
            pending=False, company_ids=[]):
        assert not ((lower_date and upper_date) and pending)
        assert not (lower_date and upper_date) or lower_date <= upper_date

        CostAllocation = Pool().get(self._get_allocation_name())

        allocs = CostAllocation.search(self._delete_allocation_domain(
            lower_date=lower_date, upper_date=upper_date, pending=pending,
            company_ids=company_ids))
        if allocs:
            CostAllocation.delete(allocs)

        srcs = self._get_allocation_sources(lower_date=lower_date,
            upper_date=upper_date, pending=pending, company_ids=company_ids)
        if not srcs:
            return

        to_create = []
        to_mark = {}
        for s in srcs:
            _allocs = self._create_allocations(s)
            if _allocs:
                to_create.extend(_allocs)
            to_mark.setdefault(s.__name__, []).append(s)

        if to_create:
            CostAllocation.create(to_create)

        for model_name, sources in to_mark.items():
            Source = Pool().get(model_name)
            Source.write(sources, {'cost_override': False})

    def _get_allocation_name(self):
        """Must return allocation model name for current procedure"""
        return ''

    def _get_allocation_sources(self, lower_date=None, upper_date=None,
            pending=False):
        """Must return source records to create allocations with"""
        return []

    def _create_allocations(self, source):
        """Must return list of new allocation dicts"""
        pass

    def _delete_allocation_domain(self, lower_date=None,
            upper_date=None, pending=False, company_ids=[]):
        """Must delete allocations for given sources
           (Remember to add cost_override and date clauses)
        """
        res = [
            ('type_', '=', self.id)
        ]
        if company_ids:
            res.append(('company', 'in', company_ids))
        return res


class CostAllocationPattern(ModelView, ModelSQL):
    """Cost allocation pattern"""

    __name__ = 'cost.manage.cost.allocation.pattern'

    code = fields.Char('Code', required=True, select=True)
    name = fields.Char('Name', required=True, translate=True, select=True)
    allocate_method = fields.Selection([
            ('quantity', 'Quantity'),
            ('time', 'Time'),
            ('quantity_time', 'Quantity & Time')
        ], 'Allocate method', required=True)

    @staticmethod
    def default_allocate_method():
        return 'quantity_time'


class CostAllocationProcedureType(ModelSQL, ModelView):
    '''Cost Allocation Procedure Type'''
    __name__ = 'cost.manage.cost.allocation.procedure.type'
    _table = 'cm_allocation_procedure_type'

    code = fields.Char('Code', required=True, select=True)
    name = fields.Char('Name', required=True, translate=True, select=True)
    procedures = fields.One2Many('cost.manage.cost.allocation.procedure',
        'procedure_type', 'Procedures')

    @classmethod
    def allocate_pending(cls, records):
        Proc = Pool().get('cost.manage.cost.allocation.procedure')
        if isinstance(records, int):
            records = [records]
        if isinstance(records, list) and isinstance(records[0], int):
            records = cls.browse(records)
        for record in records:
            if record.procedures:
                Proc.allocate_pending(record.procedures)


class CostAllocateParam(ModelView):
    '''Cost Allocate Parameters'''
    __name__ = 'cost.manage.cost.allocate.param'

    type_ = fields.Many2One('cost.manage.cost.allocation.procedure.type',
        'Type', required=True, readonly=True)
    lower_date = fields.Date('Lower date',
        states={
            'required': ~Eval('pending'),
            'invisible': Bool(Eval('pending'))},
        depends=['pending'])
    upper_date = fields.Date('Upper date',
        states={
            'required': ~Eval('pending'),
            'invisible': Bool(Eval('pending'))},
        depends=['pending'])
    procedures = fields.One2Many('cost.manage.cost.allocation.procedure',
        None, 'Procedures', domain=[('procedure_type', '=', Eval('type_'))],
        required=True, depends=['type_'])
    pending = fields.Boolean('Only pending')

    @fields.depends('pending', 'upper_date', 'lower_date')
    def on_change_pending(self):
        if self.pending:
            self.lower_date = None
            self.upper_date = None


class CostAllocate(Wizard):
    """Cost Allocate"""
    __name__ = 'cost.manage.cost.allocate'

    start = StateTransition()
    params = StateView(
        'cost.manage.cost.allocate.param',
        'cost_manage.allocate_param_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'pre_do_', 'tryton-launch', default=True)])
    pre_do_ = StateTransition()
    do_ = StateTransition()

    def transition_start(self):
        return 'params'

    def default_params(self, fields):
        Date = Pool().get('ir.date')
        return {
            'type_': self._get_procedure_type(),
            'lower_date': Date.today(),
            'upper_date': Date.today(),
            'pending': False
        }

    def _get_procedure_type(self):
        return Transaction().context['active_id']

    def _allocate(self, active_procedure):
        raise NotImplementedError

    def transition_pre_do_(self):
        return 'do_'

    def transition_do_(self):
        with Transaction().set_user(0):
            # avoid company rules
            for ap in self.params.procedures:
                self._allocate(ap)
        return 'end'
